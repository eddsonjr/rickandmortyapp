package com.edsonjr.rickandmorty.constants

class AppConstants {
    companion object {

        //constants used by endpoints
        const val RICK_MORTY_API_MAIN_URL = "https://rickandmortyapi.com/api/"
        const val CHARACTER = "character/"
        const val LOCATION = "location/"
        const val EPISODES = "episode/"


        //pager config constants
        const val PREFETCH_DISTANCE = 3
        const val CHARACTER_PAGE_SIZE  = 3


        //Character filter constants
        const val SEARCH_NAME_KEY = "name"
        const val SEARCH_STATUS_KEY = "status"
        const val SEARCH_SPECIE_KEY = "species"
        const val SEARCH_TYPE_KEY = "type"
        const val SEARCH_GENDER_KEY = "gender"


        const val UNKNOWN = "unknown"

        const val CHARACTER_TOOLBAR_NAME = "Characters"
        const val EPISODES_TOOLBAR_NAME = "Episodes"
        const val LOCATIONS_TOOLBAR_NAME = "Locations"

    }

}