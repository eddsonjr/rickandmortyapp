package com.edsonjr.rickandmorty.datasource

import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import retrofit2.Response


interface RickAndMortyFilterCharacterDataSource {

    suspend fun getCharacters(ids: List<Int>): Response<List<CharacterModel>?>

}