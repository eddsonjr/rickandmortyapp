package com.edsonjr.rickandmorty.datasource


import android.util.Log
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import com.edsonjr.rickandmorty.service.EndPoints
import retrofit2.Response

class RickAndMortyFilterCharacterDataSourceImpl(private val restAPI: EndPoints):
    RickAndMortyFilterCharacterDataSource{

    /**
     * This function requests a list of character from API
     * @param ids: List<Int>
     * @return Response<CharacterModel?>
     * */
    override suspend fun getCharacters(ids: List<Int>):
            Response<List<CharacterModel>?> = restAPI.getCharacters(ids)


}