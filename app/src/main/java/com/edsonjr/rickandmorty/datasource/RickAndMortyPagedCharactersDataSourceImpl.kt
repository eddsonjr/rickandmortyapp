package com.edsonjr.rickandmorty.datasource

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.edsonjr.rickandmorty.models.PagedResponse
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import com.edsonjr.rickandmorty.models.charactermodels.CharacterSearchFilter
import com.edsonjr.rickandmorty.service.EndPoints
import com.edsonjr.rickandmorty.utils.LogUtils
import retrofit2.Response


class RickAndMortyPagedCharactersDataSourceImpl(
    private val restAPI: EndPoints,
    private val searchFilter: CharacterSearchFilter? = null): PagingSource<Int, CharacterModel>() {

    private val TAG = this.javaClass.name

    override fun getRefreshKey(state: PagingState<Int, CharacterModel>): Int = 1

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CharacterModel> {
        val currentPageNumber = params.key ?: 1 //get current paging number or first page
        var nextPageNumber: Int? = null

        return try {

            //Check if must call getCharacters list from API or search for characters
            val response: Response<PagedResponse<CharacterModel>> = if(searchFilter != null){
                restAPI.filterCharacter(searchFilter.getQueryMap())
            }else {
                restAPI.getAllCharacters(currentPageNumber)
            }

            val pagedResponseBody = response.body()
            val responseResults = pagedResponseBody?.results //get List<Characters>

            //Logging response from api
            LogUtils.logData(TAG,"Character info from API",pagedResponseBody as PagedResponse<*>)

            //Checking if can load more pages
            if(pagedResponseBody.info.infoNextPage != null){
                val uri = Uri.parse(pagedResponseBody.info.infoNextPage)
                val nextPageQuery = uri.getQueryParameter("page")
                val tempNextPageNumber = nextPageQuery?.toInt()

                nextPageNumber = if(currentPageNumber != tempNextPageNumber)
                    nextPageQuery?.toInt()
                else
                    null

            }

            //returning a page containing data of characters
            LoadResult.Page(
                data = responseResults.orEmpty(),
                prevKey = null,
                nextKey = nextPageNumber
            )

        } catch (e: Exception) {
            LogUtils.logError(TAG,"Error when try to get characters info",e)
            LoadResult.Error(e)
        }
    }


}