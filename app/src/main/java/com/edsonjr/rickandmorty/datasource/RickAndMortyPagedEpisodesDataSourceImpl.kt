package com.edsonjr.rickandmorty.datasource

import android.net.Uri
import android.util.Log
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.edsonjr.rickandmorty.constants.AppConstants
import com.edsonjr.rickandmorty.models.PagedResponse
import com.edsonjr.rickandmorty.models.charactermodels.CharacterEpisodes
import com.edsonjr.rickandmorty.service.EndPoints
import com.edsonjr.rickandmorty.utils.LogUtils
import retrofit2.HttpException
import java.io.IOException
import java.net.UnknownHostException

class RickAndMortyPagedEpisodesDataSourceImpl(private val restAPI: EndPoints):
    PagingSource<Int, CharacterEpisodes>() {

    private val TAG = this.javaClass.name

    override fun getRefreshKey(state: PagingState<Int, CharacterEpisodes>): Int = 1

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CharacterEpisodes> {
        val currentPageNumber = params.key ?: 1 //get current paging number or first page
        var nextPageNumber: Int? = null

        return try {
            val response = restAPI.getAllEpisodes(currentPageNumber)
            val pagedResponseBody = response.body()
            val responseResults = pagedResponseBody?.results //get List<Episodes>

            //Logging response from api
            LogUtils.logData(TAG,"Episodes info from API",pagedResponseBody as PagedResponse<*>)

            //Checking if can load more pages
            if(pagedResponseBody.info.infoNextPage != null){
                val uri = Uri.parse(pagedResponseBody.info.infoNextPage)
                val nextPageQuery = uri.getQueryParameter("page")
                val tempNextPageNumber = nextPageQuery?.toInt()

                nextPageNumber = if(currentPageNumber != tempNextPageNumber)
                    nextPageQuery?.toInt()
                else
                    null

            }

            //returning a page containing data of characters
            LoadResult.Page(
                data = responseResults.orEmpty(),
                prevKey = null,
                nextKey = nextPageNumber
            )
        } catch (e: UnknownHostException){
            LoadResult.Error(e)
        } catch (e: Exception) {
            LogUtils.logError(TAG,"Error when try to get Episodes info",e)
            LoadResult.Error(e)
        } catch (e: HttpException){
            LoadResult.Error(e)
        }
    }
}