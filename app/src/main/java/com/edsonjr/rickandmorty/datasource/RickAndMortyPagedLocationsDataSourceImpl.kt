package com.edsonjr.rickandmorty.datasource

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.edsonjr.rickandmorty.models.PagedResponse
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation
import com.edsonjr.rickandmorty.service.EndPoints
import com.edsonjr.rickandmorty.utils.LogUtils

class RickAndMortyPagedLocationsDataSourceImpl(val restAPI: EndPoints):
    PagingSource<Int, CharacterLocation>(){

    private val TAG = this.javaClass.name

    override fun getRefreshKey(state: PagingState<Int, CharacterLocation>): Int = 1


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CharacterLocation> {
        val currentPageNumber = params.key ?: 1 //get current paging number or first page
        var nextPageNumber: Int? = null

        return try {
            val response = restAPI.getAllLocations(currentPageNumber)
            val pagedResponseBody = response.body()
            val responseResults = pagedResponseBody?.results //get List<Locations>

            //Logging response from api
            LogUtils.logData(TAG,"Character info from API",pagedResponseBody as PagedResponse<*>)

            //Checking if can load more pages
            if(pagedResponseBody.info.infoNextPage != null){
                val uri = Uri.parse(pagedResponseBody.info.infoNextPage)
                val nextPageQuery = uri.getQueryParameter("page")
                val tempNextPageNumber = nextPageQuery?.toInt()

                nextPageNumber = if(currentPageNumber != tempNextPageNumber)
                    nextPageQuery?.toInt()
                else
                    null

            }

            //returning a page containing data of characters
            LoadResult.Page(
                data = responseResults.orEmpty(),
                prevKey = null,
                nextKey = nextPageNumber
            )

        } catch (e: Exception) {
            LogUtils.logError(TAG,"Error when try to get locations info",e)
            LoadResult.Error(e)
        }
    }
}