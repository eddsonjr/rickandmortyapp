package com.edsonjr.rickandmorty.di

import com.edsonjr.rickandmorty.datasource.RickAndMortyFilterCharacterDataSource
import com.edsonjr.rickandmorty.datasource.RickAndMortyFilterCharacterDataSourceImpl
import com.edsonjr.rickandmorty.repository.characters.RickAndMortyCharactersRepository
import com.edsonjr.rickandmorty.repository.characters.RickAndMortyCharactersRepositoryImpl
import com.edsonjr.rickandmorty.repository.episodes.RickAndMortyEpisodeRepositoryImpl
import com.edsonjr.rickandmorty.repository.episodes.RickAndMortyEpisodesRepository
import com.edsonjr.rickandmorty.repository.locations.RickAndMortyLocationRepository
import com.edsonjr.rickandmorty.repository.locations.RickAndMortyLocationRepositoryImpl
import com.edsonjr.rickandmorty.service.EndPoints
import com.edsonjr.rickandmorty.service.RetrofitService
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyCharacterViewModel
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyEpisodesViewModel
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyLocationViewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object DependencyInjection {

    //DI of retrofit service
    private val retrofitServiceModule = module {
        single { RetrofitService().initRetrofit() }
        single<EndPoints>{get<Retrofit>().create(EndPoints::class.java)}
    }


    //DI of ViewModel
    private val viewModelModule = module {
        single { RickAndMortyCharacterViewModel(get()) }
        single { RickAndMortyEpisodesViewModel(get())  }
        single { RickAndMortyLocationViewModel(get())  }
    }


    //DI of DataSource layer
    private val dataSourceModules = module {
        factory  <RickAndMortyFilterCharacterDataSource> { RickAndMortyFilterCharacterDataSourceImpl(get()) }
    }

    //DI of repository
    private val repositoryModules = module {
        factory<RickAndMortyCharactersRepository> { RickAndMortyCharactersRepositoryImpl(get(),get()) }
        factory<RickAndMortyEpisodesRepository> { RickAndMortyEpisodeRepositoryImpl(get()) }
        factory<RickAndMortyLocationRepository> { RickAndMortyLocationRepositoryImpl(get()) }
    }


    //list of all modules
    val appModules = listOf(
        retrofitServiceModule,
        dataSourceModules,
        repositoryModules,
        viewModelModule)

}