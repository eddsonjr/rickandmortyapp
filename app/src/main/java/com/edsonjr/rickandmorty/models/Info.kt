package com.edsonjr.rickandmorty.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Info(

    @SerializedName("count")
    val infoCount: Int,

    @SerializedName("pages")
    val infoPages: Int,

    @SerializedName("next")
    val infoNextPage: String?,

    @SerializedName("prev")
    val infoPrevPage: String?

): Serializable