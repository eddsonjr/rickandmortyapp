package com.edsonjr.rickandmorty.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class PagedResponse<T>(
    @SerializedName("info")
    val info: Info,

    @SerializedName("results")
    val results: List<T>
): Serializable