package com.edsonjr.rickandmorty.models.charactermodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CharacterEpisodes(

    @SerializedName("id")
    val episodeId: Int,

    @SerializedName("name")
    val episodeName: String,

    @SerializedName("air_date")
    val air_date: String,

    @SerializedName("episode")
    val episodeCode: String,

    @SerializedName("created")
    val episodeCreationDate: String

): Serializable