package com.edsonjr.rickandmorty.models.charactermodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class CharacterLocation(
    @SerializedName("name")
    val locationName: String?,

    @SerializedName("type")
    val type: String?,

    @SerializedName("dimension")
    val dimension: String?,

    @SerializedName("url")
    val locationUrl: String?

): Serializable