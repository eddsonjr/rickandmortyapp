package com.edsonjr.rickandmorty.models.charactermodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable



data class CharacterOrigin(
    @SerializedName("name")
    val originName: String,

    @SerializedName("url")
    val originUrl: String
): Serializable