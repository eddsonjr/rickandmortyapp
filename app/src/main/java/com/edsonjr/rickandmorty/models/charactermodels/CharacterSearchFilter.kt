package com.edsonjr.rickandmorty.models.charactermodels

import com.edsonjr.rickandmorty.constants.AppConstants.Companion.SEARCH_GENDER_KEY
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.SEARCH_NAME_KEY
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.SEARCH_SPECIE_KEY
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.SEARCH_STATUS_KEY
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.SEARCH_TYPE_KEY
import com.edsonjr.rickandmorty.utils.LogUtils
import java.io.Serializable

const val TAG = "search_filters"

data class CharacterSearchFilter(
    val name: String = "",
    val status: String = "",
    val specie: String = "",
    val type: String = "",
    val gender: String = ""
): Serializable {

    /**
     * Use this function to create and return HashMap used by filterCharacter()
     * @return Map<String?,String?>
     * */
    fun getQueryMap(): Map<String?,String?> {
        val map: HashMap<String?,String?> = HashMap()
        map[SEARCH_NAME_KEY] = name
        map[SEARCH_STATUS_KEY] = status
        map[SEARCH_SPECIE_KEY] = specie
        map[SEARCH_GENDER_KEY] = gender
        map[SEARCH_TYPE_KEY] = type

        LogUtils.logData(TAG,"Character filters",map)
        return map
    }
}


class CharacterStatus {
    companion object {
        const val DEAD = "dead"
        const val ALIVE = "alive"
        const val UNKNOWN = "unknown"
    }
}

class CharacterGender {
    companion object {
        const val FEMALE = "female"
        const val MALE = "male"
        const val GENDERLESS = "genderless"
        const val UNKNOWN = "unknown"
    }
}