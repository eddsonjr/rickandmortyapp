package com.edsonjr.rickandmorty.repository.characters

import androidx.paging.PagingData
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import com.edsonjr.rickandmorty.models.charactermodels.CharacterSearchFilter
import com.edsonjr.rickandmorty.service.NetworkResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

interface RickAndMortyCharactersRepository {

    fun getAllCharacters(): Flow<PagingData<CharacterModel>>

    fun getFilteredCharacter(searchFilter: CharacterSearchFilter): Flow<PagingData<CharacterModel>>
    
    suspend fun getCharactersByIDs(ids: List<Int>): Flow<NetworkResult<List<CharacterModel>?>>


}