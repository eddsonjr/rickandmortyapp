package com.edsonjr.rickandmorty.repository.characters

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.CHARACTER_PAGE_SIZE
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.PREFETCH_DISTANCE
import com.edsonjr.rickandmorty.datasource.RickAndMortyFilterCharacterDataSource
import com.edsonjr.rickandmorty.datasource.RickAndMortyPagedCharactersDataSourceImpl
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import com.edsonjr.rickandmorty.models.charactermodels.CharacterSearchFilter
import com.edsonjr.rickandmorty.service.BaseApiResponse
import com.edsonjr.rickandmorty.service.EndPoints
import com.edsonjr.rickandmorty.service.NetworkResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class RickAndMortyCharactersRepositoryImpl(
    private val restAPI: EndPoints,
    private val filteredDataSource: RickAndMortyFilterCharacterDataSource
    ): RickAndMortyCharactersRepository,BaseApiResponse() {
    

    /**
     * This function requests a list of all characters from DataSource.
     * @return  Flow<PagingData<CharacterModel>>
     * */
    override fun getAllCharacters(): Flow<PagingData<CharacterModel>>{
         return configCharacterPage().flow //transform a pager into a flow of pager data
    }


    /**
     * This function request a list of filtered characters from DataSource.
     * @param searchFilter: CharacterSearchFilter
     * @return  Flow<PagingData<CharacterModel>>
     * */
    override fun getFilteredCharacter(searchFilter: CharacterSearchFilter):
            Flow<PagingData<CharacterModel>>  {
        return configCharacterPage(searchFilter).flow
    }



    /**
     * This function request a list of characters from DataSource based on his ids.
     * @param ids: List<Int>
     * @return   Flow<NetworkResult<CharacterModel?>>
     * */
    override suspend fun getCharactersByIDs(ids: List<Int>): Flow<NetworkResult<List<CharacterModel>?>> {
        return flow{
            emit(safeApiCall { filteredDataSource.getCharacters(ids) })
        }.flowOn(Dispatchers.IO)
    }



    /**
     * This function configures the pager used by PagingLib v3.
     * Used by getAllCharacters (get list of characters and search for characters)
     * */
    private fun configCharacterPage(searchFilter: CharacterSearchFilter? = null) =  Pager(
        config = PagingConfig(
            pageSize = CHARACTER_PAGE_SIZE,
            prefetchDistance = PREFETCH_DISTANCE),
        pagingSourceFactory = {
            RickAndMortyPagedCharactersDataSourceImpl(
            restAPI = restAPI,
            searchFilter = searchFilter)
        }
    )

}