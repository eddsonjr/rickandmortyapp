package com.edsonjr.rickandmorty.repository.episodes

import androidx.paging.*
import com.edsonjr.rickandmorty.constants.AppConstants
import com.edsonjr.rickandmorty.datasource.RickAndMortyPagedEpisodesDataSourceImpl
import com.edsonjr.rickandmorty.models.charactermodels.CharacterEpisodes
import com.edsonjr.rickandmorty.service.EndPoints
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

class RickAndMortyEpisodeRepositoryImpl(private val restAPI: EndPoints): RickAndMortyEpisodesRepository {


    /**
     * This function requests a list of all episodes from DataSource.
     * @return  Flow<PagingData<CharacterEpisodes>>
     * */
    override fun getAllEpisodes(): Flow<PagingData<CharacterEpisodes>> {
        return configEpisodesPages().flow
    }


    /**
     * This function configures the pager used by PagingLib v3.
     * Used by getAllEpisodes (get list of episodes and search for characters)
     * */
    private fun configEpisodesPages() =  Pager(
        config = PagingConfig(
            pageSize = AppConstants.CHARACTER_PAGE_SIZE,
            prefetchDistance = AppConstants.PREFETCH_DISTANCE
        ),
        pagingSourceFactory = {
            RickAndMortyPagedEpisodesDataSourceImpl(
                restAPI = restAPI)
        }
    )


}