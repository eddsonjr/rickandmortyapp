package com.edsonjr.rickandmorty.repository.episodes

import androidx.paging.PagingData
import com.edsonjr.rickandmorty.models.charactermodels.CharacterEpisodes
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow


interface RickAndMortyEpisodesRepository {

    fun getAllEpisodes(): Flow<PagingData<CharacterEpisodes>>
}