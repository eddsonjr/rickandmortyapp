package com.edsonjr.rickandmorty.repository.locations

import androidx.paging.PagingData
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

interface RickAndMortyLocationRepository {

    fun getAllLocations(): Flow<PagingData<CharacterLocation>>
}