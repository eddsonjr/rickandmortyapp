package com.edsonjr.rickandmorty.repository.locations

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.edsonjr.rickandmorty.constants.AppConstants
import com.edsonjr.rickandmorty.datasource.RickAndMortyPagedLocationsDataSourceImpl
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation
import com.edsonjr.rickandmorty.service.EndPoints
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

class RickAndMortyLocationRepositoryImpl(private val restAPI: EndPoints):
    RickAndMortyLocationRepository {


    /**
     * This function requests a list of all locations from DataSource
     * @return Flow<PagingData<CharacterLocation>>
     * */
    override fun getAllLocations():
            Flow<PagingData<CharacterLocation>> {
        return configCharacterPage().flow
    }


    /**
     * This function configures the pager used by PagingLib v3.
     * Used by getAllLocations (get list of locations)
     * */
    private fun configCharacterPage() =  Pager(
        config = PagingConfig(
            pageSize = AppConstants.CHARACTER_PAGE_SIZE,
            prefetchDistance = AppConstants.PREFETCH_DISTANCE
        ),
        pagingSourceFactory = {
            RickAndMortyPagedLocationsDataSourceImpl(restAPI = restAPI)
        }
    )
}