package com.edsonjr.rickandmorty.service

import androidx.paging.PagingData
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.CHARACTER
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.EPISODES
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.LOCATION
import retrofit2.http.GET
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.RICK_MORTY_API_MAIN_URL
import com.edsonjr.rickandmorty.models.PagedResponse
import com.edsonjr.rickandmorty.models.charactermodels.CharacterEpisodes
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import retrofit2.Response
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap


private const val CHARACTER_URL = RICK_MORTY_API_MAIN_URL + CHARACTER
private const val LOCATION_URL = RICK_MORTY_API_MAIN_URL + LOCATION
private const val EPISODE_URL = RICK_MORTY_API_MAIN_URL + EPISODES

interface EndPoints {

    /**
     * It gets the list of all Rick And Morty characters.
     * This endpoint is based on pages
     * @return Response<PagedResponse<CharacterModel>>
     * */
    @GET(CHARACTER_URL)
    suspend fun getAllCharacters(
        @Query("page") page: Int): Response<PagedResponse<CharacterModel>>


    /**
     * It gets a list of chracaters by ids
     * @param ids: List<Int>
     * @return Response<CharacterModel?>
     * */
    @GET("$CHARACTER_URL{listIDs}")
    suspend fun getCharacters(
        @Path("listIDs") ids: List<Int>): Response<List<CharacterModel>?>



    /**
     * Make a search on API and gets a list of paginate results.
     * @param filters: Map<String,String>
     * @return Response<PagedResponse<CharacterModel>>
     * */
    @GET(CHARACTER_URL)
    suspend fun filterCharacter(
        @QueryMap filters: Map<String?, String?>
    ):Response<PagedResponse<CharacterModel>>




    /**
     * It gets the list of all Rick And Morty locations.
     * This endpoint is based on pages
     * @return Response<PagedResponse<CharacterLocation>>
     * */
    @GET(LOCATION_URL)
    suspend fun getAllLocations(
        @Query("page") page: Int): Response<PagedResponse<CharacterLocation>>




    /**
     * It gets the list of alL Rick and Morty episodes
     * This endpoint is based on pages
     * @return Response<PagedResponse<CharacterEpisodes>>
     * */
    @GET(EPISODE_URL)
    suspend fun getAllEpisodes(
        @Query("page") page: Int): Response<PagedResponse<CharacterEpisodes>>



}