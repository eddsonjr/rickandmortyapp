package com.edsonjr.rickandmorty.service


sealed class NetworkResult<T>(
    val data: T? = null,
    val errorMsg: String? = null)
    {
        class Success<T>(data: T) : NetworkResult<T>(data)
        class Error<T>(errorMsg: String, exception: T? = null) : NetworkResult<T>(exception, errorMsg)
    }