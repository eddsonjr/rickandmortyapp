package com.edsonjr.rickandmorty.service

import com.edsonjr.rickandmorty.constants.AppConstants.Companion.RICK_MORTY_API_MAIN_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitService {

    private var logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val client = OkHttpClient.Builder()

    fun initRetrofit (): Retrofit {

        //configure logging for retrofit 2
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        client.addInterceptor(logging)
            .connectTimeout(30,TimeUnit.SECONDS)
            .build()


        return Retrofit.Builder()
            .baseUrl(RICK_MORTY_API_MAIN_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }
}