package com.edsonjr.rickandmorty.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {


        fun convertData(dateStr: String): String{
            val serverFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            val currentDateFormat = "yyyy/MM/dd"

            val serverDateFormat = SimpleDateFormat(serverFormat, Locale.US)
            val newDateFormat = SimpleDateFormat(currentDateFormat)

            val serverDateObj = serverDateFormat.parse(dateStr)
            val outputDateText = newDateFormat.format(serverDateObj)

            return outputDateText.toString()
        }

    }
}