package com.edsonjr.rickandmorty.utils

import android.util.Log
import androidx.paging.PagingData
import com.edsonjr.rickandmorty.models.PagedResponse
import java.lang.Exception

class LogUtils {
    companion object {


        /**
         * This function logs paginated information (printing Info and Results).
         * You can use this function to log API paginated responses (i.e)
         * @param className: String
         * @param customMsg: String
         * @param pagedData: PagedResponse<T>?
         * */
        fun <T>logData(
            className: String,
            customMsg: String,
            pagedData: PagedResponse<T>?){
                val msg = """
                    $customMsg
                    Info: ${pagedData?.info.toString()}
                    results: ${pagedData?.results.toString()}
                """.trimIndent()
                Log.d(className,msg)
        }




        /**
         * This function logs information about any object.
         * Use this to log about data class infos, maps, and others (i.e)
         * @param className: String
         * @param customMsg: String
         * @param obj: T
         * */
        fun <T>logData(
            className: String,
            customMsg: String,
            obj: T
        ) {
            val msg = """
                    $customMsg
                    data: ${obj.toString()}
                """.trimIndent()
            Log.d(className,msg)

        }



        /**
         * This function logs about erros.
         * @param className: String
         * @param customErrorMsg: String?
         * @param e: Exception
         * */
        fun logError(
            className: String,
            customErrorMsg: String? = null,
            e: Exception? = null){

                if(!customErrorMsg.isNullOrEmpty())
                    Log.e(className,customErrorMsg)

                Log.e(className,e?.message.toString())
                Log.e(className,e?.printStackTrace().toString())

        }
    }
}