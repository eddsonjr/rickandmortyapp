package com.edsonjr.rickandmorty.utils


sealed class SearchStateUtils {
    data class SEARCH_FOR_ID(val data: List<Int>):  SearchStateUtils()
    object SEARCH_FOR_NAME: SearchStateUtils()
    data class ERROR(val e: Throwable): SearchStateUtils()
}