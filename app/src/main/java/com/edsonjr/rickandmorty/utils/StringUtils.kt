package com.edsonjr.rickandmorty.utils

import android.util.Log


const val COMMA_SEPARATOR = ","
const val SEPARATOR_REGEX = ",{2,}"
const val WHITE_SPACE_REGEX = "\\s"
const val SEARCH_FOR_ID_REGEX = "([0-9]+(,?)[0-9]?)+(,?)+"
class StringUtils {

    companion object {

        private val TAG = this.javaClass.name

        /**
         * Convert a string in format "1,2,3,4..." to list of int
         * @param param:String
         * @return List<Int>
         * */
        private fun convertSearchParamToListOfIDs(param: String): List<Int> =
            param.prepareToGenerateList().split(COMMA_SEPARATOR).map { it.toInt() }



        /**
         * This function verifies if user is searching for IDs or not
         * @param param: String,
         * @return Boolean
         * */
        private fun checkIfSearchForIDsOrNot(param: String): Boolean =
            param.contains(SEARCH_FOR_ID_REGEX.toRegex())



        /**
         * This function checks if user is searching for character's name or character's ids. It
         * will returns the search state or error.
         * In the case of search by IDs, the state encapsulates the list of IDs
         * In the case of Error, the state encapsulates the error.
         * This function uses SearchStateUtils for control of search states
         *
         * @param param:String
         * @return SearchStateUtils
         * */
        fun checkSearchCharacter(param: String) =
            if(!checkIfSearchForIDsOrNot(param)) SearchStateUtils.SEARCH_FOR_NAME
            else {
                try{
                    val listOfIds = convertSearchParamToListOfIDs(param)
                    SearchStateUtils.SEARCH_FOR_ID(listOfIds)
                }catch (e: NumberFormatException){
                    SearchStateUtils.ERROR(e)
                }
            }
    }

}



/**
 * This string extension checks the for whitespaces and commas and remove them.
 *@return String
 * */
fun String.prepareToGenerateList(): String {
    val TAG = "String Extension: "
    var s = this

    //checking for whitespaces
    if(s.contains(WHITE_SPACE_REGEX.toRegex())){
        Log.d(TAG,"Search parameter contains whitespaces. Replacing for ,")
        s = this.replace(WHITE_SPACE_REGEX.toRegex(),",")

    }

    //checking for (,){2,} regex
    if(s.contains(SEPARATOR_REGEX.toRegex())){
        Log.d(TAG,"Search parameter contains (,){2,} regex, replacing it for ,")
        s = s.replace(SEPARATOR_REGEX.toRegex(),",")
    }

    //checking if string contains , at the end
    if(s.last().toString() == COMMA_SEPARATOR){
        Log.d(TAG,"Search parameter contains , at the end. Removing it")
        s = s.substring(0,s.length-1)
    }
    return s
}
