package com.edsonjr.rickandmorty.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.edsonjr.rickandmorty.R
import com.edsonjr.rickandmorty.databinding.ActivityMainBinding
import com.edsonjr.rickandmorty.view.adapters.CharacterPagedAdapter
import com.edsonjr.rickandmorty.view.adapters.EpisodesAdapter
import com.edsonjr.rickandmorty.view.adapters.LocationAdapter
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyCharacterViewModel
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyEpisodesViewModel
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyLocationViewModel
import com.edsonjr.rickandmorty.viewmodel.ViewState
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    //TODO - SOMENTE PARA TESTES
    private val characterViewModel: RickAndMortyCharacterViewModel by viewModel()
    private val episodeViewModel: RickAndMortyEpisodesViewModel by viewModel()
    private val locationViewModel: RickAndMortyLocationViewModel by viewModel()


    private val pagedAdapter: CharacterPagedAdapter = CharacterPagedAdapter()
    private val episodeAdapter: EpisodesAdapter = EpisodesAdapter()
    private val locationAdapter: LocationAdapter = LocationAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //configure bottom navigation bar
        setupNavigation()

    }


    private fun setupNavigation(){
        val navcontroller = Navigation.findNavController(this, R.id.navigation_fragment_container)
        NavigationUI.setupWithNavController(binding.bottomNavigationBar,navcontroller)
    }


}