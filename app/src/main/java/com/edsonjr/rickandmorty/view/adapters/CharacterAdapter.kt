package com.edsonjr.rickandmorty.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.rickandmorty.databinding.ItemCharacterBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel


//This adapter is used by non paging search, like search for IDs
class CharacterAdapter:RecyclerView.Adapter<CharacterViewHolder>() {

    private var listOfCharacters: MutableList<CharacterModel>? = mutableListOf()

    var clickCallBack:(CharacterModel) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCharacterBinding.inflate(layoutInflater,parent,false)
        return CharacterViewHolder(binding,clickCallBack)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bind(listOfCharacters?.get(position)!!)
    }

    override fun getItemCount(): Int = listOfCharacters?.size ?: 0


    fun updateCharactersAdapter(charactersList: List<CharacterModel>?){
        if(listOfCharacters?.isNotEmpty() == true)
            listOfCharacters?.clear()

        listOfCharacters = charactersList?.toMutableList()
        notifyDataSetChanged()
    }
}