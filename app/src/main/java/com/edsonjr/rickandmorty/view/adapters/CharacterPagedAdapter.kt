package com.edsonjr.rickandmorty.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.rickandmorty.databinding.ItemCharacterBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel


//This adapter is used for paging data
class CharacterPagedAdapter: PagingDataAdapter<CharacterModel,RecyclerView.ViewHolder>(COMPARATOR) {

    var clickCallBack:(CharacterModel) -> Unit = {}

    //diffutils comparator
    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<CharacterModel>() {
            override fun areItemsTheSame(
                oldItem: CharacterModel,
                newItem: CharacterModel
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: CharacterModel,
                newItem: CharacterModel
            ): Boolean {
                return oldItem == newItem
            }

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewBinding = ItemCharacterBinding
            .inflate(LayoutInflater.from(parent.context),parent,false)
        return CharacterViewHolder(viewBinding,clickCallBack)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let { (holder as CharacterViewHolder).bind(it) }
    }
}