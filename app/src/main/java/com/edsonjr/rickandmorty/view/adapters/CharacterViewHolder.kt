package com.edsonjr.rickandmorty.view.adapters

import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.edsonjr.rickandmorty.R
import com.edsonjr.rickandmorty.databinding.ItemCharacterBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel


class CharacterViewHolder(private val view: ItemCharacterBinding,
                          private var clickCallBack:(CharacterModel) -> Unit = {}):
    RecyclerView.ViewHolder(view.root) {

    fun bind(item: CharacterModel) {

        //load the character image
        view.characterImg.load(item.characterImage) {
            crossfade(true)
            placeholder(R.drawable.ic_no_image)
        }

        view.characterNameTxt.text = item.characterName
        view.characterGenderTxt.text = item.characterGender

        view.root.setOnClickListener {
            clickCallBack(item)
        }

    }
}