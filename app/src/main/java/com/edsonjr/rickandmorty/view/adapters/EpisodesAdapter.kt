package com.edsonjr.rickandmorty.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.rickandmorty.databinding.ItemEpisodeBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterEpisodes
import com.edsonjr.rickandmorty.utils.DateUtils

class EpisodesAdapter: PagingDataAdapter<CharacterEpisodes,RecyclerView.ViewHolder>(COMPARATOR) {


    inner class CharacterEpisodeViewHolder(private val view: ItemEpisodeBinding):
        RecyclerView.ViewHolder(view.root){

            fun bind(item: CharacterEpisodes){
                view.itemEpisodeIdTxt.text = item.episodeId.toString()
                view.itemEpisodeCodeTxt.text = item.episodeCode
                view.itemEpisodeNameTxt.text = item.episodeName
                view.itemEpisodeAirdateTxt.text = item.air_date
                view.itemEpisodeLaunchedText.text = DateUtils.convertData(item.episodeCreationDate)

            }

        }


    //diffutils comparator
    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<CharacterEpisodes>() {
            override fun areItemsTheSame(
                oldItem: CharacterEpisodes,
                newItem: CharacterEpisodes
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: CharacterEpisodes,
                newItem: CharacterEpisodes
            ): Boolean {
                return oldItem == newItem
            }


        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let { (holder as CharacterEpisodeViewHolder).bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewBinding = ItemEpisodeBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return CharacterEpisodeViewHolder(viewBinding)
    }


}