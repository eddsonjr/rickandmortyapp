package com.edsonjr.rickandmorty.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.rickandmorty.databinding.ItemCharacterEpisodesCharacterDetailsBinding

class EpisodesAdapterCharacterDetailsAdapter:RecyclerView.Adapter<EpisodesCharacterDetailsViewHolder>() {

    private var listOfLocations: MutableList<String> = mutableListOf()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EpisodesCharacterDetailsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCharacterEpisodesCharacterDetailsBinding.inflate(
            layoutInflater,parent,false)
        return EpisodesCharacterDetailsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EpisodesCharacterDetailsViewHolder, position: Int) {
        holder.bind(listOfLocations[position])
    }

    override fun getItemCount(): Int = listOfLocations.size


    fun updateEpisodeAdapter(episodesList: List<String>){
        if(listOfLocations.isNotEmpty())
            listOfLocations.clear()

        listOfLocations = episodesList.toMutableList()
        notifyDataSetChanged()
    }


}