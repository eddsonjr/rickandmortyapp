package com.edsonjr.rickandmorty.view.adapters

import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.rickandmorty.databinding.ItemCharacterEpisodesCharacterDetailsBinding

class EpisodesCharacterDetailsViewHolder(
    private val view: ItemCharacterEpisodesCharacterDetailsBinding):
    RecyclerView.ViewHolder(view.root){

        fun bind(episodeName: String) {
            view.epsideNameTextview.text = episodeName
        }
}