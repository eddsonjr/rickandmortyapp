package com.edsonjr.rickandmorty.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.UNKNOWN
import com.edsonjr.rickandmorty.databinding.ItemLocationBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation


class LocationAdapter: PagingDataAdapter<CharacterLocation, RecyclerView.ViewHolder>(COMPARATOR) {


    inner  class CharacterLocationViewHolder(private val view: ItemLocationBinding):
        RecyclerView.ViewHolder(view.root){

            fun bind(item: CharacterLocation){
                if(item.dimension.isNullOrEmpty())
                    view.locationDimensionTxt.text = UNKNOWN
                else
                    view.locationDimensionTxt.text = item.dimension

                view.locationNameTxt.text = item.locationName
                view.locationTypeTxt.text = item.type
            }
        }


    //diffutils comparator
    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<CharacterLocation>() {
            override fun areItemsTheSame(
                oldItem: CharacterLocation,
                newItem: CharacterLocation
            ): Boolean {
               return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: CharacterLocation,
                newItem: CharacterLocation
            ): Boolean {
                return oldItem == newItem
            }


        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {(holder as CharacterLocationViewHolder).bind(it)}
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewBinding = ItemLocationBinding
            .inflate(LayoutInflater.from(parent.context),parent,false)
        return CharacterLocationViewHolder(viewBinding)

    }
}