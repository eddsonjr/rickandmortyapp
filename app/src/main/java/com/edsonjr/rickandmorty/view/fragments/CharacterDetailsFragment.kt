package com.edsonjr.rickandmorty.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toolbar
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.edsonjr.rickandmorty.R
import com.edsonjr.rickandmorty.constants.AppConstants
import com.edsonjr.rickandmorty.databinding.FragmentCharacterDetailsBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import com.edsonjr.rickandmorty.utils.DateUtils
import com.edsonjr.rickandmorty.view.adapters.EpisodesAdapterCharacterDetailsAdapter


class CharacterDetailsFragment : Fragment() {

    private val TAG = this.javaClass.name

    private val args: CharacterDetailsFragmentArgs by navArgs()
    private lateinit var characterModel: CharacterModel
    private lateinit var adapter: EpisodesAdapterCharacterDetailsAdapter

    private var _binding: FragmentCharacterDetailsBinding? = null
    private val binding get() =  _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        characterModel = args.characterModelArgument
        Log.d(TAG,"Character selected: $characterModel")
        _binding = FragmentCharacterDetailsBinding.inflate(inflater,container,false)
        setupUI()
        setHasOptionsMenu(true)
        initToolbar()
        return  binding?.root
    }


    override fun onStop() {
        super.onStop()
        _binding = null
    }



    private fun setupUI() {

        //load the character image
        binding?.characterImage?.load(characterModel.characterImage) {
            crossfade(true)
            placeholder(R.drawable.ic_no_image)
        }

        binding?.characaterNameTextview?.text = characterModel.characterName

        if(characterModel.characterGender.isNotBlank())
            binding?.characterGenderTextview?.text = characterModel.characterGender
        else
            binding?.characterGenderTextview?.text = setToUnknownInfo()


        if(characterModel.characterType.isNotBlank())
            binding?.characterTypeTextview?.text = characterModel.characterType
        else
            binding?.characterTypeTextview?.text = setToUnknownInfo()


        if(characterModel.characterStatus.isNotBlank())
            binding?.characterStatusTextview?.text = characterModel.characterStatus
        else
            binding?.characterStatusTextview?.text = setToUnknownInfo()


        binding?.characterLocationTextview?.text = characterModel.characterLocation.locationName
            ?: requireActivity().resources.getText(R.string.unknown_location)


        if(characterModel.characterCreated.isNotBlank())
            binding?.characterCreationDateTextview?.text = DateUtils.convertData(characterModel.characterCreated)
        else
            binding?.characterCreationDateTextview?.text = setToUnknownInfo()

        if(characterModel.characterSpecies.isNotBlank())
            binding?.characterSpecieTextview?.text = characterModel.characterSpecies
        else
            binding?.characterSpecieTextview?.text = setToUnknownInfo()


        if(characterModel.characterOrigin.originName.isNotBlank())
            binding?.characterOriginsTextview?.text = characterModel.characterOrigin.originName
        else
            binding?.characterOriginsTextview?.text = setToUnknownInfo()

        setupRecyclerView()
    }


    private fun setupRecyclerView() {
        adapter = EpisodesAdapterCharacterDetailsAdapter()
        binding?.characterDetailsEpisodesRecyclerview?.layoutManager = LinearLayoutManager(requireContext())
        binding?.characterDetailsEpisodesRecyclerview?.adapter = adapter
        adapter.updateEpisodeAdapter(characterModel.characterEpisodes)

    }


    private fun setToUnknownInfo(): CharSequence {
        return requireActivity().resources.getText(R.string.unknown_info)
    }


    //region - setup toolbar
    private fun initToolbar() {
        val rootView = binding?.root
        val toolbar = rootView?.findViewById<Toolbar>(R.id.toolbar)

        if(!characterModel.characterName.isNullOrEmpty()){
            toolbar?.title = characterModel.characterName
        }else {
            toolbar?.title =
                requireActivity().resources.getString(R.string.character_detail_fragment_default_name)
        }
        toolbar?.inflateMenu(R.menu.toolbar_character_details_menu)
        requireActivity().setActionBar(toolbar)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.toolbar_character_details_menu, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId)
        {
            R.id.back_to_character_fragment_menu -> {
                findNavController().popBackStack()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    //endregion

}