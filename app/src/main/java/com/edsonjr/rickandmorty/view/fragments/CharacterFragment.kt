package com.edsonjr.rickandmorty.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import com.edsonjr.rickandmorty.R
import com.edsonjr.rickandmorty.constants.AppConstants.Companion.CHARACTER_TOOLBAR_NAME
import com.edsonjr.rickandmorty.databinding.FragmentCharacterBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import com.edsonjr.rickandmorty.view.adapters.CharacterAdapter
import com.edsonjr.rickandmorty.view.adapters.CharacterPagedAdapter
import com.edsonjr.rickandmorty.viewmodel.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class CharacterFragment : Fragment() {

    private val TAG = this.javaClass.name

    private var _binding: FragmentCharacterBinding? = null
    private val binding get() = _binding!!

    private val characterViewModel: RickAndMortyCharacterViewModel by sharedViewModel()

    private val pagedAdapter = CharacterPagedAdapter()
    private val nonPagedAdapter = CharacterAdapter()


    private val args: CharacterFragmentArgs by navArgs()
    private var characterSearchParams: CharacterSearchParamTransferData? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCharacterBinding.inflate(inflater, container, false)
        initToolbar()
        setHasOptionsMenu(true)
        getCharacterSearchParamsViaArgs()
        return binding.root
    }


    override fun onResume() {
        super.onResume()
        handleSearchCharactersByIDs()
    }

    override fun onStop() {
        super.onStop()
        _binding = null
    }


    private fun getCharacterSearchParamsViaArgs() {
        if(args.searchParamsData != null){
            args.searchParamsData?.let { characterSearchParams = it }
            procedureWithSearch()
        }else{
            handleGetAllCharacters()
        }
    }


    private fun procedureWithSearch() {
        if(characterSearchParams?.encapsulatedData is CharacterSearchParams){
            Log.d(TAG,"Searching for character's name / specie / type / gender ...")
            characterSearchParams?.encapsulatedData?.let {
                val encapsulatedData = it as CharacterSearchParams
                handleSearchCharactersByParams(encapsulatedData)
            }

        }else if (characterSearchParams?.encapsulatedData is CharacterSearchListOfIDs){
            Log.d(TAG, "Searching for IDs of characters...")
            characterSearchParams?.encapsulatedData?.let {
                val encapsulatedData = it as CharacterSearchListOfIDs
                characterViewModel.searchCharactersForListOfIDs(encapsulatedData.ids)
            }
        }

    }


    //region - handle requests
    private fun handleGetAllCharacters() {
        val charactersPagedData = characterViewModel.getAllCharacters()
        lifecycleScope.launch{
            charactersPagedData.collectLatest {
                setupCharacterPagedAdapter(it)
            }
        }
    }


    private fun handleSearchCharactersByParams(params: CharacterSearchParams){

        //Clearing adapters data
        pagedAdapter.submitData(this.lifecycle,PagingData.empty())
        nonPagedAdapter.updateCharactersAdapter(null)
        val charactersPagedData = characterViewModel.searchCharactersByFilters(params)

        lifecycleScope.launch {
            charactersPagedData.collectLatest { pagedData ->
                setupCharacterPagedAdapter(pagedData)
            }
        }
    }

    private fun handleSearchCharactersByIDs() {
        characterViewModel.searchCharactersForIDsViewState.observe(viewLifecycleOwner) { state ->
            when(state){
                is ViewState.LOADING -> {

                    //Clearing adapters data
                    pagedAdapter.submitData(this.lifecycle,PagingData.empty())
                    nonPagedAdapter.updateCharactersAdapter(null)

                    binding.characterShimmerProgress.visibility = View.VISIBLE
                    binding.errorViewFramelayout.visibility = View.GONE
                    binding.characterShimmerProgress.startShimmer()
                }
                is ViewState.SUCCESS -> {
                    binding.characterShimmerProgress.stopShimmer()
                    binding.characterShimmerProgress.visibility = View.GONE
                    binding.errorViewFramelayout.visibility = View.GONE
                    val data = state.data
                    data?.let { setupCharacterNonPagedAdapter(it) }
                }
                is ViewState.ERROR -> {
                    binding.characterShimmerProgress.stopShimmer()
                    binding.characterShimmerProgress.visibility = View.GONE
                    binding.errorViewFramelayout.visibility = View.VISIBLE
                }
            }

        }
    }

    //endregion




    //region - setup character adapters
    //Paged adapter for getAllCharacters or searchByFilters
    private fun setupCharacterPagedAdapter(pagedData: PagingData<CharacterModel>) {
        binding.characterRecyclerview.layoutManager = GridLayoutManager(requireContext(),2)
        binding.characterRecyclerview.adapter = pagedAdapter
        checkPagingStates()

        lifecycleScope.launch{
            pagedAdapter.submitData(pagedData)
        }

        //Character's adapter click callback.
        //Navigate to CharacterDetailFragment and pass to it the one CharacterModel object.
        pagedAdapter.clickCallBack = { characterModel ->
            goToCharacterDetailsFragment(characterModel)
        }
    }


    //Non paged adapter for search character by IDs
    private fun setupCharacterNonPagedAdapter(listOfCharacters: List<CharacterModel>){
        binding.characterRecyclerview.layoutManager = GridLayoutManager(requireContext(),2)
        binding.characterRecyclerview.adapter = nonPagedAdapter
        nonPagedAdapter.updateCharactersAdapter(listOfCharacters)
        nonPagedAdapter.clickCallBack = { characterModel ->
            goToCharacterDetailsFragment(characterModel)
        }

    }



    //controls the view state based by paging object returned
    private fun checkPagingStates(){
        lifecycleScope.launch {
            pagedAdapter.loadStateFlow.collect { loadStates ->
                when(loadStates.refresh){
                    is LoadState.NotLoading -> {
                        Log.d(TAG,"Episodes data loaded successfully...")
                        binding.characterShimmerProgress.stopShimmer()
                        binding.errorViewFramelayout.visibility = View.GONE
                        binding.characterShimmerProgress.visibility = View.GONE
                    }

                    is LoadState.Error -> {
                        Log.d(TAG,"Error when get episodes data...")
                        binding.characterShimmerProgress.stopShimmer()
                        binding.characterShimmerProgress.visibility = View.GONE
                        binding.errorViewFramelayout.visibility = View.VISIBLE
                    }

                    is LoadState.Loading -> {
                        Log.d(TAG,"Loading episodes data...")
                        binding.errorViewFramelayout.visibility = View.GONE
                        binding.characterShimmerProgress.startShimmer()
                        binding.characterShimmerProgress.visibility = View.VISIBLE
                    }
                }
            }
        }
    }



    //endregion


    //region - setup toolbar
    private fun initToolbar() {
        val rootView = binding.root
        val toolbar = rootView.findViewById<Toolbar>(R.id.toolbar)
        toolbar.inflateMenu(R.menu.toolbar_character_menu)
        toolbar.title = CHARACTER_TOOLBAR_NAME
        requireActivity().setActionBar(toolbar)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.toolbar_character_menu, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId)
        {
            R.id.search_character_menu -> {
                findNavController().navigate(R.id.action_characterFragment_to_searchSetupBottomSheet)
                return true
            }
            R.id.reload_character_menu -> {
                nonPagedAdapter.updateCharactersAdapter(null)
                pagedAdapter.submitData(this.lifecycle,PagingData.empty())
                handleGetAllCharacters()
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun goToCharacterDetailsFragment(characterSelected: CharacterModel){
        val toDetailsAction = CharacterFragmentDirections
            .actionCharacterFragmentToCharacterDetailsFragment(characterSelected)
        findNavController().navigate(toDetailsAction)
    }

    //endregion
}
