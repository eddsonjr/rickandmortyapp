package com.edsonjr.rickandmorty.view.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.rickandmorty.R
import com.edsonjr.rickandmorty.constants.AppConstants
import com.edsonjr.rickandmorty.databinding.FragmentEpisodesBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterEpisodes
import com.edsonjr.rickandmorty.utils.LogUtils
import com.edsonjr.rickandmorty.view.adapters.EpisodesAdapter
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyEpisodesViewModel
import com.edsonjr.rickandmorty.viewmodel.ViewState
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class EpisodesFragment : Fragment() {

    private val TAG = this.javaClass.name

    private var _binding: FragmentEpisodesBinding? = null
    private val binding get() = _binding!!

    private val adapter = EpisodesAdapter()
    private val episodeViewModel: RickAndMortyEpisodesViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEpisodesBinding.inflate(inflater,container,false)
        initToolbar()
        setHasOptionsMenu(true);
        return binding.root
    }


    override fun onResume() {
        super.onResume()
        handleGetAllEpisodes()

    }

    override fun onStop() {
        super.onStop()
        _binding = null
    }


    private fun handleGetAllEpisodes() {
        val episodePagedData = episodeViewModel.getAllEpisodes()
        lifecycleScope.launch {
            episodePagedData.collectLatest {
                setupEpisodeRecyclerView(it)
            }
        }
    }


    //region - setup toolbar
    private fun initToolbar() {
        val rootView = binding.root
        val toolbar = rootView.findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = AppConstants.EPISODES_TOOLBAR_NAME
        requireActivity().setActionBar(toolbar)
    }
    //endregion



    //region - recyclerview setup and loadstates listener
    private fun setupEpisodeRecyclerView(pagingData: PagingData<CharacterEpisodes>) {
        binding.episodesRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.episodesRecyclerView.adapter = adapter
        checkPagingStates()
        lifecycleScope.launch {
           adapter.submitData(pagingData)
        }
    }


    //controls the view state based by paging object returned
    private fun checkPagingStates(){
        lifecycleScope.launch {
            adapter.loadStateFlow.collect { loadStates ->
                when(loadStates.refresh){
                    is LoadState.Error -> {
                        Log.d(TAG,"Error when get episodes data...")
                        binding.episodesShimmerProgress.stopShimmer()
                        binding.episodesShimmerProgress.visibility = View.GONE
                        binding.errorViewFramelayout.visibility = View.VISIBLE
                    }

                    is LoadState.Loading -> {
                        Log.d(TAG,"Loading episodes data...")
                        binding.errorViewFramelayout.visibility = View.GONE
                        binding.episodesShimmerProgress.startShimmer()
                        binding.episodesShimmerProgress.visibility = View.VISIBLE
                    }

                    is LoadState.NotLoading -> {
                        Log.d(TAG,"Episodes data loaded successfully...")
                        binding.errorViewFramelayout.visibility = View.GONE
                        binding.episodesShimmerProgress.stopShimmer()
                        binding.episodesShimmerProgress.visibility = View.GONE
                    }
                }
            }
        }
    }




}