package com.edsonjr.rickandmorty.view.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.rickandmorty.R
import com.edsonjr.rickandmorty.constants.AppConstants
import com.edsonjr.rickandmorty.databinding.FragmentLocationsBinding
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation
import com.edsonjr.rickandmorty.utils.LogUtils
import com.edsonjr.rickandmorty.view.adapters.LocationAdapter
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyLocationViewModel
import com.edsonjr.rickandmorty.viewmodel.ViewState
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class LocationsFragment : Fragment() {

    private val TAG = this.javaClass.name

    private var _binding: FragmentLocationsBinding? = null
    private val binding get() = _binding!!

    private val adapter = LocationAdapter()
    private val viewModel: RickAndMortyLocationViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLocationsBinding.inflate(inflater,container,false)
        initToolbar()
        setHasOptionsMenu(true);
        return binding.root
    }


    override fun onResume() {
        super.onResume()
        handleGetAllLocations()
        checkPagingStates()
    }


    override fun onStop() {
        super.onStop()
        _binding = null
    }

    private fun handleGetAllLocations() {
        val pagedLocationData = viewModel.getAllLocations()
        lifecycleScope.launch{
            pagedLocationData.collectLatest {
                setupLocationRecyclerView(it)
            }
        }
    }


    private fun setupLocationRecyclerView(pagedData: PagingData<CharacterLocation>) {
        binding.locationsRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.locationsRecyclerView.adapter = adapter
        lifecycleScope.launch {
            adapter.submitData(pagedData)
        }
    }



    //controls the view state based by paging object returned
    private fun checkPagingStates(){
        lifecycleScope.launch {
            adapter.loadStateFlow.collect { loadStates ->
                when(loadStates.refresh){
                    is LoadState.Error -> {
                        Log.d(TAG,"Error when get episodes data...")
                        binding.locationShimmerProgress.stopShimmer()
                        binding.locationShimmerProgress.visibility = View.GONE
                        binding.errorViewFramelayout.visibility = View.VISIBLE
                    }

                    is LoadState.Loading -> {
                        Log.d(TAG,"Loading episodes data...")
                        binding.errorViewFramelayout.visibility = View.GONE
                        binding.locationShimmerProgress.startShimmer()
                        binding.locationShimmerProgress.visibility = View.VISIBLE
                    }

                    is LoadState.NotLoading -> {
                        Log.d(TAG,"Episodes data loaded successfully...")
                        binding.errorViewFramelayout.visibility = View.GONE
                        binding.locationShimmerProgress.stopShimmer()
                        binding.locationShimmerProgress.visibility = View.GONE
                    }
                }
            }
        }
    }


    //region - setup toolbar
    private fun initToolbar() {
        val rootView = binding.root
        val toolbar = rootView.findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = AppConstants.LOCATIONS_TOOLBAR_NAME
        requireActivity().setActionBar(toolbar)
    }

    //endregion

}