package com.edsonjr.rickandmorty.view.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.core.text.isDigitsOnly
import androidx.core.view.forEach
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.navigation.fragment.findNavController
import com.edsonjr.rickandmorty.R
import com.edsonjr.rickandmorty.databinding.FragmentSearchSetupBottomSheetBinding
import com.edsonjr.rickandmorty.utils.COMMA_SEPARATOR
import com.edsonjr.rickandmorty.utils.SearchStateUtils
import com.edsonjr.rickandmorty.utils.StringUtils
import com.edsonjr.rickandmorty.utils.StringUtils.Companion.checkSearchCharacter
import com.edsonjr.rickandmorty.viewmodel.CharacterSearchListOfIDs
import com.edsonjr.rickandmorty.viewmodel.CharacterSearchParamTransferData
import com.edsonjr.rickandmorty.viewmodel.CharacterSearchParams
import com.edsonjr.rickandmorty.viewmodel.RickAndMortyCharacterViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.io.Serializable
import java.util.*


class SearchSetupBottomSheet : BottomSheetDialogFragment() {

    private val TAG = this.javaClass.name

    private var _binding: FragmentSearchSetupBottomSheetBinding? = null
    private val binding get() = _binding!!

    private var searchCharacterGenderParam: String = ""
    private var searchCharacterStatusParam: String = ""
    private var nameOrIDsSearchFieldText: String = ""
    private var specieSearchFieldText: String = ""
    private var typeSearchFieldText: String = ""

//    private val characterViewModel: RickAndMortyCharacterViewModel by sharedViewModel()
    private var searchParamsTransferData: CharacterSearchParamTransferData? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchSetupBottomSheetBinding.inflate(inflater,container,false)
        searchCharacterGenderChangeListener()
        searchCharacterStatusChangeListener()
        setupDoneButtonClickListener()
        configureSearchFields()
        return binding.root
    }



    //This function configures the listener of Gender Chip Group
    private fun searchCharacterGenderChangeListener() {
        binding.searchGenderChipgroup.setOnCheckedChangeListener { group, selectedChiId ->
            val selectedChip = group.findViewById<Chip>(selectedChiId)
            searchCharacterGenderParam = selectedChip.text.toString().lowercase(Locale.ROOT).orEmpty()
            Log.d(TAG,"Character gender selected: $searchCharacterGenderParam")
        }
    }


    //This funcion configures the listener of Status Chip Group
    private fun searchCharacterStatusChangeListener() {
        binding.searchStatusChipGroup.setOnCheckedChangeListener { group, selectedChipId ->
            val selectedChip = group.findViewById<Chip>(selectedChipId)
            searchCharacterStatusParam = selectedChip.text.toString().lowercase(Locale.ROOT).orEmpty()
            Log.d(TAG,"Character status selected: $searchCharacterStatusParam")

        }
    }


    //This function configures the listeners of all search text edits
    private fun configureSearchFields() {

        //Name or ID search edit text
        binding.characterNameSearchEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) { nameOrIDsSearchFieldText = s.toString() }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //Nothing  to do ...
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val str = s.toString()

                if(str.isBlank()){
                    activeOrDeactiveChipGroups(true)
                    activeOrDeactiveSearchFields(true)
                }else {
                    if(str.isDigitsOnly() || str.contains(COMMA_SEPARATOR)){
                        activeOrDeactiveChipGroups(false)
                        activeOrDeactiveSearchFields(false)
                    }
                    if(!str.isDigitsOnly() && !str.contains(COMMA_SEPARATOR) && str.isNotBlank()){
                        activeOrDeactiveChipGroups(true)
                        activeOrDeactiveSearchFields(true)
                    }


                    nameOrIDsSearchFieldText = str

                }

            }
        })


        //Specie search edit text
        binding.characterSpecieSearchEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) { specieSearchFieldText = s.toString() }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //Nothing  to do ...
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                specieSearchFieldText = s.toString()
            }
        })



        //Type search edit text
        binding.characterTypeSearchEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) { typeSearchFieldText = s.toString() }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //Nothing  to do ...
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                typeSearchFieldText = s.toString()
            }
        })
    }



    //This function enable or deactivate Gender And Status chip groups
    private fun activeOrDeactiveChipGroups(active: Boolean){
        binding.searchGenderChipgroup.forEach { chipItemView ->
            chipItemView.isEnabled = active
            if(!active){
                chipItemView.alpha = 0.5f
            }else{
                chipItemView.alpha = 1.0f
            }
        }

        binding.searchStatusChipGroup.forEach { chipItemView ->
            chipItemView.isEnabled = active
            if(!active){
                chipItemView.alpha = 0.5f
            }else{
                chipItemView.alpha = 1.0f
            }
        }
    }

    private fun activeOrDeactiveSearchFields(active: Boolean){
        binding.characterTypeSearchEditText.isEnabled = active
        binding.characterSpecieSearchEditText.isEnabled = active

        if(active){
            binding.typeLabel.alpha = 1.0f
            binding.specieLabel.alpha = 1.0f
        }else{
            binding.typeLabel.alpha = 0.5f
            binding.specieLabel.alpha = 0.5f
        }

    }


    //Done click listener
    private fun setupDoneButtonClickListener() {
        binding.executeCharaterSearchBtn.setOnClickListener {
            checkSearchParametersAndExecuteSearchRequest()
        }
    }



    //This function checks and configures character parameters
    private fun checkSearchParametersAndExecuteSearchRequest() {
        if(nameOrIDsSearchFieldText.isEmpty() && searchCharacterGenderParam.isEmpty()
            && searchCharacterStatusParam.isEmpty() && typeSearchFieldText.isEmpty()
            && specieSearchFieldText.isEmpty()){

            AlertDialog.Builder(requireContext())
                .setTitle(requireContext().getString(R.string.dialog_atention_title))
                .setMessage(requireContext().getString(R.string.search_alert_change_fields))
                .setPositiveButton(requireContext().getString(R.string.Ok),null)
                .show()

        }else{
            val state = checkSearchCharacter(nameOrIDsSearchFieldText)
            when(state){
                is SearchStateUtils.SEARCH_FOR_NAME -> {
                    Log.d(TAG,"User searching for name")
                    val searchParams = CharacterSearchParams(
                        name = nameOrIDsSearchFieldText,
                        status = searchCharacterStatusParam,
                        gender = searchCharacterGenderParam,
                        specie = specieSearchFieldText,
                        type = typeSearchFieldText
                    )
                    Log.d(TAG,"Search params: $searchParams")
                    searchParamsTransferData = CharacterSearchParamTransferData(
                        encapsulatedData = searchParams
                    )
                    navigateToCharacterFragment(searchParamsTransferData)
//                    characterViewModel.searchParamsLiveData.value = searchParams
//                    dismiss()
                }
                is SearchStateUtils.SEARCH_FOR_ID -> {
                    val listOfIDs = state.data
                    Log.d(TAG, "User searching for Ids")
                    Log.d(TAG,"List of IDs to search: $listOfIDs")

                    val listOfIDsSearchParam = CharacterSearchListOfIDs(listOfIDs)
                    searchParamsTransferData = CharacterSearchParamTransferData(
                        encapsulatedData = listOfIDsSearchParam
                    )
                    navigateToCharacterFragment(searchParamsTransferData)
//                    characterViewModel.searchListIDsLiveData.value = listOfIDsSearchParam
//                    dismiss()
                }
                is SearchStateUtils.ERROR -> {
                    val error = state.e
                    Log.e(TAG,"Error when do search for characters...")
                    Log.e(TAG,error.message.toString())
                    Log.e(TAG,error.printStackTrace().toString())

                    AlertDialog.Builder(requireContext())
                        .setTitle(requireContext().getString(R.string.dialog_atention_title))
                        .setMessage(requireContext().getString(R.string.search_alert_error))
                        .setPositiveButton(requireContext().getString(R.string.Ok),null)
                        .show()
                }
            }
        }
    }


    private fun navigateToCharacterFragment(dataToPass: CharacterSearchParamTransferData?){
        val action = SearchSetupBottomSheetDirections
            .actionSearchSetupBottomSheetToCharacterFragment().setSearchParamsData(dataToPass)
        findNavController().navigate(action)
    }


    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Exiting from BottomSheetDialogFragment() ")
        _binding = null
    }

}