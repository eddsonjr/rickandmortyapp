package com.edsonjr.rickandmorty.viewmodel

import java.io.Serializable


data class CharacterSearchParams(
    val name: String =  "",
    val specie: String =  "",
    val type: String =  "",
    val status: String =  "",
    val gender: String =  ""
): Serializable



data class CharacterSearchListOfIDs(
    val ids: List<Int>
): Serializable



//Data class to transfer data between  SearchSetupBottomSheet and CharacterFragment
data class CharacterSearchParamTransferData(
    val encapsulatedData: Serializable? = null
): Serializable