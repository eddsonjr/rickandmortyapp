package com.edsonjr.rickandmorty.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.edsonjr.rickandmorty.models.charactermodels.CharacterModel
import com.edsonjr.rickandmorty.models.charactermodels.CharacterSearchFilter
import com.edsonjr.rickandmorty.repository.characters.RickAndMortyCharactersRepository
import com.edsonjr.rickandmorty.service.NetworkResult
import com.edsonjr.rickandmorty.utils.LogUtils
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class RickAndMortyCharacterViewModel(private val repository: RickAndMortyCharactersRepository):
    ViewModel(){

    private val TAG = this.javaClass.name

    //It controls the search characters based on IDs states
    private val _searchCharactersForIDsViewState = MutableLiveData<ViewState<List<CharacterModel>?>>()
    val searchCharactersForIDsViewState: LiveData<ViewState<List<CharacterModel>?>>
        get() = _searchCharactersForIDsViewState




    /**
     * This method requests a list of all characters from repository (paged request).
     * @return Flow<PagingData<CharacterModel>> (cached)
     * */
    @SuppressLint("CheckResult")
    fun getAllCharacters(): Flow<PagingData<CharacterModel>>{
        return repository.getAllCharacters().cachedIn(viewModelScope)
    }


    /**
     *This method requests a list of filtered characters from repository (paged data)
     *@param characterSearchParams: CharacterSearchParams
     * @return Flow<PagingData<CharacterModel>>
     * */
    fun searchCharactersByFilters(characterSearchParams: CharacterSearchParams):
            Flow<PagingData<CharacterModel>> {
        val characterFilter = CharacterSearchFilter(
            name = characterSearchParams.name,
            specie = characterSearchParams.specie,
            type = characterSearchParams.type,
            status = characterSearchParams.status,
            gender =  characterSearchParams.gender
        )
        return repository.getFilteredCharacter(characterFilter).cachedIn(viewModelScope)
    }




    /**
     * Request a search of characters based on lit of character's IDs.
     * @param ids: List<Int>
     * */
    fun searchCharactersForListOfIDs(ids: List<Int>) = viewModelScope.launch {

        _searchCharactersForIDsViewState.postValue(ViewState.LOADING)

        repository.getCharactersByIDs(ids).collect { data ->
            when(data){
                is NetworkResult.Success -> {
                    LogUtils.logData(TAG,"Get characters returned successfully",data.data)
                    _searchCharactersForIDsViewState.postValue(ViewState.SUCCESS(data.data))
                }
                is NetworkResult.Error -> {
                    LogUtils.logError(TAG,data.errorMsg)
                    _searchCharactersForIDsViewState.postValue(ViewState.ERROR)
                }
            }
        }
    }
}