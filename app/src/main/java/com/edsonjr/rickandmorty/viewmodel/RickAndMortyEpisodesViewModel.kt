package com.edsonjr.rickandmorty.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.edsonjr.rickandmorty.models.charactermodels.CharacterEpisodes
import com.edsonjr.rickandmorty.repository.episodes.RickAndMortyEpisodesRepository
import kotlinx.coroutines.flow.*

class RickAndMortyEpisodesViewModel(private val repository: RickAndMortyEpisodesRepository):
    ViewModel() {


    /**
     * This method requests a list of all episodes from repository
     * @return Flow<PagingData<CharacterEpisodes>>
     * */
    fun getAllEpisodes(): Flow<PagingData<CharacterEpisodes>>{
        return repository.getAllEpisodes().cachedIn(viewModelScope)
    }

}