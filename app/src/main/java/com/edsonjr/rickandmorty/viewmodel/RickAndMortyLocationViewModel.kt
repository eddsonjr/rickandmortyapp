package com.edsonjr.rickandmorty.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.edsonjr.rickandmorty.models.charactermodels.CharacterLocation
import com.edsonjr.rickandmorty.repository.locations.RickAndMortyLocationRepository
import kotlinx.coroutines.flow.*

class RickAndMortyLocationViewModel(private val repository: RickAndMortyLocationRepository):
    ViewModel() {

    /**
     * This method requests a list of all locations from repository
     * @return Flow<PagingData<CharacterLocation>>
     * */
    fun getAllLocations(): Flow<PagingData<CharacterLocation>> {
        return  repository.getAllLocations().cachedIn(viewModelScope)

    }
}