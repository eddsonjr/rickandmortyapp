# Rick and Morty App

Aplicação Android/Kotlin que consome a API do Rick And Morty e que oferece ao usuário várias informações a respeito dos personagens, episódios e ambientação das séries do desenho Rick and Morty. 

---


## API
 
Você pode acessar a API clicando [aqui](https://rickandmortyapi.com/).
Todas as requisições são do tipo __GET__ e são feitas através de requisições https. Todas as respostas da API serão retornadas em formato json. A API também trabalha com dados páginados. 

Os principais endpoints são: 

__Character__:

```
https://rickandmortyapi.com/api/character
```


__Locations__:

```
https://rickandmortyapi.com/api/location
```



__Locations__:

```
https://rickandmortyapi.com/api/episode
```


### API - Characters

A resposta vinda da API para personagens: 

```
GET https://rickandmortyapi.com/api/character

{
  "info": {
    "count": 826,
    "pages": 42,
    "next": "https://rickandmortyapi.com/api/character/?page=2",
    "prev": null
  },
  "results": [
    // ...
  ]
}
```

Por trabalhar com paginação, você pode selecionar qual página de dados gostaria de acessar e o retorno será equivalente a:

```
GET https://rickandmortyapi.com/api/character/?page=19

{
  "info": {
    "count": 826,
    "pages": 42,
    "next": "https://rickandmortyapi.com/api/character/?page=20",
    "prev": "https://rickandmortyapi.com/api/character/?page=18"
  },
  "results": [
    {
      "id": 361,
      "name": "Toxic Rick",
      "status": "Dead",
      "species": "Humanoid",
      "type": "Rick's Toxic Side",
      "gender": "Male",
      "origin": {
        "name": "Alien Spa",
        "url": "https://rickandmortyapi.com/api/location/64"
      },
      "location": {
        "name": "Earth",
        "url": "https://rickandmortyapi.com/api/location/20"
      },
      "image": "https://rickandmortyapi.com/api/character/avatar/361.jpeg",
      "episode": [
        "https://rickandmortyapi.com/api/episode/27"
      ],
      "url": "https://rickandmortyapi.com/api/character/361",
      "created": "2018-01-10T18:20:41.703Z"
    },
    // ...
  ]
}

```


Caso tente acessar uma página que não existe, receberá um:

```
error	"There is nothing here"
```


Todos os demais endpoints desta API trabalham de forma semelhante. 


---

## Tecnologias e Arquitetura

O aplicativo conta com o uso das seguintes tecnologias e arquitetura:
 - Arquitetura MVVM com padrão Repository. 
 - Kotlin Flow
 - Retrofit / OKHttp / GSON
 - Fragments /  Navigation Component / BottomSheetDialogFragment 
 - Toolbar customizada para cada fragment
 - Koin (injeção de dependências)
 - Paging V3 lib
 - View State


## Explanações sobre as tecnologias e a arquitetura

### MVVM

O MVVM é um padrão de arquitetura de desenvolvimento de software baseado em Model, View e View-Model. Conheça a seguir os três principais elementos: 
 - **Model**: Camada de modelo e lógica de negócios que impulsiona a aplicação.
 - **View**: Camada destinada a interface com usuário (UI).
 - **ViewModel**: Camada intermediária entre _View_ e _Model_, servindo como um elo de ligação entre ambas. O objetivo desta camada é responder aos inputs do usuário e providenciar as chamadas para a camada de Model e manter a camada de View sempre atualizada. 

Para maiores detalhes veja [este artigo aqui](https://coodesh.com/blog/dicionario/o-que-e-arquitetura-mvvm/) 


### Padrão Repository 

O padrão _Repository_ tem como principal objetivo a abstração da aquisição e/ou persistência dos dados. Ele serve para isolar a responsabilidade de aquisição (e salvamento, caso necessário) dos dados das demais classes, tais como entidades. A grande vantagem disso é que as classes que trabalham com regras de negócio não precisam se preocupar em como adquirir tais dados, sendo esta uma função delegada diretamente ao _repository_. Isso permite o desacoplamento e também a facilidade da troca de tecnologias com relação à aquisição dos dados (ex: trocar o retrofit por outra lib) e/ou salvamento (ex: trocar o Room por Realm). 

O padrão _repository_ pode vir também atrelado a um _Data Source_. Geralmente se utiliza _data sources_ quando se há mais de uma maneira de adquirir os dados, como por exemplo, através de uma requisição web ou via banco de dados local. 


O MVVM com Repository, de forma esquemática fica:

![alt](https://miro.medium.com/max/960/1*-yY0l4XD3kLcZz0rO1sfRA.png)


### Retrofit e GSON

**Retrofit** - Biblioteca para trabalhar com requisições web REST API. Veja mais na documentação da [Retrofit](https://square.github.io/retrofit/)

**GSon** - Biblioteca utilizada para serializar e desserializar objetos Kotlin/Java para JSON e vice versa. Consulte a documentação da [Gson](https://github.com/google/gson) para maiores detalhes.




### Navigation Component 
É uma das bibliotecas do Android JetPack  e oferece facilidades no que diz respeito ao trabalho com Fragments. 
Para maiores detalhes veja [a documentação official](https://developer.android.com/guide/navigation/navigation-getting-started) e [este artigo](https://medium.com/android-dev-br/usando-navigation-component-android-jetpack-e1356921b31d).



### Koin 
 
O Koin é um framework responsável por auxiliar na injeção de dependências. A documentação oficial consta [aqui](https://insert-koin.io/docs/quickstart/android).


### Flow (Kotlin Flow)

De acordo com a documentação oficial: 

> Um Flow  é um tipo que pode emitir vários valores sequencialmente, ao contrário das funções de suspensão, que retornam somente um valor. Por exemplo, você pode usar um fluxo para receber atualizações em tempo real de um banco de dados.
> Os Flows são criados com base nas corrotinas e podem fornecer vários valores. Conceitualmente, um Flow é um stream de dados que pode ser computado de forma assíncrona. Os valores emitidos precisam ser do mesmo tipo. Por exemplo, um Flow<Int> é um fluxo que emite valores inteiros.
> Um Flow é muito semelhante a um Iterator que produz uma sequência de valores, mas usa funções de suspensão para produzir e consumir valores de maneira assíncrona. Isso significa, por exemplo, que o Flow pode fazer uma solicitação de rede com segurança para produzir o próximo valor sem bloquear a linha de execução principal.


Você pode ver mais detalhes da documentação da google clicando [aqui](https://developer.android.com/kotlin/flow). 
Caso queira saber mais operações sobre Kotlin Flow, [veja](https://kotlinlang.org/docs/flow.html#intermediate-flow-operators). 

Você também pode combinar chamadas e streams com o Kotlin Flow, utilizando vários métodos relacionados, tais como o _flatMapMerge()_, _flatMap()_ e _flatMapLatest()_. Para maiores detalhes, veja  [este artigo](https://kt.academy/article/cc-flatmap).


### View State
Você pode utilizar uma técnica de View State em conjunto com Flow  para controlar o estado da _View_. Para tanto, pode-se criar a seguinte classe para controlar os estados da _View_: 

```
//ViewState generico
sealed class ViewState<out T> {
    data class SUCCESS<T> (val data: T?): ViewState<T>()
    object  ERROR: ViewState<Nothing>()
    object LOADING: ViewState<Nothing>()
}

```

No _ViewModel_, pode-se utilizar esta classe de acordo com as respostas advindas pelo Flow: 

```
    //Atencao: forma generica abaixo. Leve em consideracao os processamentos necessarios no viewmodel  do app!
    

    //As variaveis abaixo serao observadas na camada de View
    private val _controlViewState = MutableLiveData<ViewState<List<Model>?>>()
    val controlViewState: LiveData<ViewState<List<Model>?>>
        get() = _controlViewState


    
    fun someFunctionCall() = viewModelScope.launch {
        
        repository.callRepositorySomeFunction()
            .onStart { //flow detecta que comecara a execucao e stream de dados
                _controlViewState.postValue(ViewState.LOADING)
            }
            .onEach { //flow adquire os dados 
                _controlViewState.postValue(ViewState.SUCCESS(it))
            }
            .catch {  //tratamento de erro via flow
                _controlViewState.postValue(ViewState.ERROR))
            }  
     }
    

```

Já na sua camada de _View_, observe o _ViewState_: 

```

    val myViewModel: MyViewModel by viewModel() 

    fun observeViewState() {
        
        myViewModel.observe(viewLifecycleOwner) { state -> 
            when(state) {
                is ViewState.LOADING -> { //estado de loading da tela}
                is ViewState.SUCCESS -> { //prepare a UI com os dados para o usuario}
                is ViewState.ERROR -> { //houve alugm erro. }  
            }

        }
    }

```


Você pode coletar os dados de um flow  com _collect{}_ e o _collectLatest{}_ além de usar o _.onEach{}_.

Você pode criar vários tipos de estados dentro do seu ViewState e trabalhá-los de acordo com as respostas que são do servidor / banco de dados.


### Paging Lib 

É uma biblioteca do Jetpack que auxilia no desenvolvimento da camada de _View_, no que diz respeito a carga de dados paginados. Geralmente a paginação é um recurso utilizado quando se trafega uma grande quantidade de informações entre dois pontos, como por exemplo uma API e um App. 

Esta biblioteca já abstrai as operações de verificação de carga de dados e checagem de rolagem do adapter, coisa que tem que ser feita manualmente caso se opte por não utilizá-la.  Perceba que por conta desta abstração, esta biblioteca interage com determinados componentes específicos e apresenta um meio peculiar de desenvolvimento de _DataSource_ e um meio específico de controle dos estados na camada de _View_. 

Os componentes utilizados na Paging Lib, são:
 - **PagingSource**: Define uma fonte de dados e como recuperar os dados de tal fonte. 
 - **RemoteMediator (opcional)**: Um objeto deste tipo processa a paginação de uma fonte de dados em camadas. Geralmente é utilizado quando se tem dados paginados advindos da rede e com cache em banco de dados. 
 - **Pager**: Cria as instâncias de PagingData e é através do Pager que se define e configura questões relacionadas a carga dos dados, como por exemplo, tamanho da paginação e fonte dos dados. 
 - **PagingData**: Container no qual encapusla os dados páginados em si. 
 - **PagingDataAdapter**: é um adapter específico para recyclerview.


A paging pode ser utilizada em conjunto com Flow e LiveData, além de RXJava segundo a documentação oficial.

É importante ter em mente que a submissão dos dados para dentro do PaingDataAdapter deve ser feita dentro de um escopo de Coroutine:

```
    lifecycleScope.launch{
            pagedAdapter.submitData(pagedData)
    }

``` 

Já a manipulação dos estados da _View_ pela Pagging pode ser feito através do _CombinedLoadStates_. Seu uso com Flow segue o padrão descrito abaixo (obs:  perceba que existem vários tipos de estados e que os mesmo são combinados entre si): 

```
     lifecycleScope.launch {
            pagedAdapter.loadStateFlow.collect { loadStates ->
                when(loadStates.refresh){
                    is LoadState.NotLoading -> {
                        //Dados paginados carregados
                    }

                    is LoadState.Error -> {
                       //Ocorreu um erro durante a carga dos dados paginados
                    }

                    is LoadState.Loading -> {
                        //Carregadno os dados paginados
                    }
                }
            }
``` 

Para maiores informações a respeito da Pagging e adaptadores concatenados, veja [este artigo](https://medium.com/swlh/paging3-recyclerview-pagination-made-easy-333c7dfa8797), [esta documentação](https://developer.android.com/topic/libraries/architecture/paging/v3-paged-data), [adapters concatenados](https://wajahatkarim.com/2022/06/concat-adapter/). 






